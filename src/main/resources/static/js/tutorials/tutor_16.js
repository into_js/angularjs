var app = angular
    .module('tutor_16', [])
    .controller('tutor_16', function ($scope) {

        $scope.master = {};

        // $scope.update = function(user) {
        //     console.log($scope.form.uCode.$error);
        //     $scope.master = angular.copy(user);
        // };

        $scope.update = {
            submit: function (user) {
                console.info(user);
                $scope.master = angular.copy(user);
            }
        };

        $scope.reset = function() {
            $scope.user = angular.copy($scope.master);
        };

        $scope.reset();

    });

app.directive("httpCodesOnly", function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, mCtrl) {
            function myValidator(value) {
                console.log('validation test');
                if (value >= 100 && value <= 599) {
                    mCtrl.$setValidity('httpCodesOnly', true);
                } else {
                    mCtrl.$setValidity('httpCodesOnly', false);
                }
                return value;
            }
            mCtrl.$parsers.push(myValidator);
        }
    }
});
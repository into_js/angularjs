var app = angular
    .module('tutor_15_2', [])
    .controller('tutor_15_2', function ($scope) {

        var countries = [
            {
                name: "India",
                cities: [
                    { name: "Hyderabad" },
                    { name: "Chennai" }
                ]
            },
            {
                name: "USA",
                cities: [
                    { name: "Los Angeles" },
                    { name: "Chicago" },
                ]
            }
        ];

        $scope.countries = countries;
    });

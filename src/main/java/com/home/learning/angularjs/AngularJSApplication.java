package com.home.learning.angularjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularJSApplication {

    public static void main(String[] args) {
        SpringApplication.run(AngularJSApplication.class, args);
    }

}

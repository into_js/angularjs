package com.home.learning.angularjs.controllers.tutorial;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tutor")
public class AngularJsTutorials {

    /**
     * custom filters in angularJs
     */
    @GetMapping("/13")
    public String tutor13() {
        return "tutorials/tutor_13";
    }

    /**
     * ng hide and show
     */
    @GetMapping("/14")
    public String tutor14() {
        return "tutorials/tutor_14";
    }

    /**
     * ng hide and show for masking
     */
    @GetMapping("/14_2")
    public String tutor14_2() {
        return "tutorials/tutor_14_masking";
    }

    /**
     * ng init directive
     */
    @GetMapping("/15")
    public String tutor15() {
        return "tutorials/tutor_15";
    }

    /**
     * ng init directive
     */
    @GetMapping("/15_2")
    public String tutor15_2() {
        return "tutorials/tutor_15_2";
    }


    /**
     * custom validation
     */
    @GetMapping("/16")
    public String tutor16() {
        return "tutorials/tutor_16";
    }

}
